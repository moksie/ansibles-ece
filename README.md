# Introduction #


### What is this repository for? ###

This repository contains an ansible project to provide automation around preparing and managing developer test servers for Escenic Content Engine powered web applications. This will provision your test server with the following without any manual intervention. 

* You can configure any number of escenic instances where, each such instance may have 
  + Different versions of escenic content engine
  + Any combination of plugins with desired versions (possibly different instance to instance) 
  + Any number of publication webapps
  + Different application servers
  + Different java versions
  + Many more
* Right now, you can run only one instance of escenic at a time unless you manually provisioned the server differently

### Requirements ###
* One or more target machines having ***ssh access***. 
* Operating system of those machines should be preferably ***Ubuntu*** (or any debian based linux)
* You should have installed and configured ***ansible*** on your controller machine.

You may take a look at the scripts in the ```contrib``` folder for an overview about installing and configuring **ansible** on you controller machine if you are using **Ubuntu**. You may simply run the ```setup-ansibles.sh```. But before that, don't forget to update the **ip addresses** mentioned in the script. Those are basically the ip address of the target machines and come in handy if you create and manage your virtual machines using ```setup-vagrant.sh```. 

 
### Basic Usage ###
Assuming that, you have installed and configured ansible on your developer machine and can access the target test machines using ssh the following commands should be enough to prepare your escenic host.

```
#!shell

$ ansible-playbook get-builder-set.yml -e "host=ece-host"
$ ansible-playbook get-ecehost-set.yml -e "host=ece-host"
```

Now, if you run the following command it will install all the escenic components required for the instance ```tr```

```
#!shell
$ ansible-playbook go.yml -e "host=ece-host instance=tr" 
```

Now, you should be able to login to that test machine (``` ece-host ```) using the credential ```escenic:escenic```. Login to that machine with this credential and run the ```ece``` script for the instance ```tr```. For example,
```
#!shell

$ ece -i tr clean assemble stop deploy start outlog
```

For more information about the available commands, run the following
```
$ ece -i tr help
```

### Adding / Modifying the configuration for instances ###
Coming soon ...