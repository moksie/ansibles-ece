#!/usr/bin/env bash

work_dir=~/ansibles/ece

# add required packages
apt-get update
apt-get install -y python-software-properties
apt-add-repository -y ppa:rquillo/ansible
apt-get update
apt-get install -y ansible sshpass git

# Create inventory file for ansible
echo "localhost ansible_connection=local

builder ansible_ssh_host=192.168.43.10 ansible_ssh_user=escenic ansible_ssh_pass=escenic ansible_sudo_pass=escenic
ecehost ansible_ssh_host=192.168.43.10 ansible_ssh_user=escenic ansible_ssh_pass=escenic ansible_sudo_pass=escenic

" > /etc/ansible/hosts

# download required playbook
test -d $work_dir || ( mkdir -p $work_dir && chmod 777 $work_dir)
cd $work_dir
git clone https://bitbucket.org/moksie/ansibles-ece.git || echo "Already cloned the project"