#!/usr/bin/env bash

work_dir=~/vagrant/escenic-host

# Install virtual box
echo "deb http://download.virtualbox.org/virtualbox/debian trusty contrib" | sudo tee -a /etc/apt/sources.list > /dev/null
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

sudo apt-get update

sudo apt-get install -y linux-headers-3.16.0-34-generic virtualbox-4.3
sudo /etc/init.d/vboxdrv setup

# Install vagrant
wget https://dl.bintray.com/mitchellh/vagrant/vagrant_1.7.2_x86_64.deb -O /tmp/vagrant_1.7.2_x86_64.deb
sudo dpkg --install /tmp/vagrant_1.7.2_x86_64.deb


# Setup escenic host working directory
test -d $work_dir || ( mkdir -p $work_dir && chmod 777 $work_dir)
cd $work_dir

echo '

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/trusty64"
  config.vm.box_check_update = true

  config.vm.hostname = "escenic-host"


  config.vm.network "private_network", ip: "192.168.43.10"
#  config.vm.network "public_network"
  config.vm.synced_folder "~/", "/mnt/vagrant_shared_data"

 config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 2
 end


 config.vm.provision "shell", inline: <<-SHELL
   sudo apt-get update
   sudo apt-get install -y apache2
   sudo useradd -G sudo -p ecpf7xJtUQs8o -s /bin/bash -m escenic
 SHELL

end

' > $work_dir/Vagrantfile